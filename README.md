# eyetracking_analysis
Code and sample data for Psycholinguistics final project, working with eye tracking data from Jeff Witzel and Naoko Witzel at UT Arlington.

The data included here is **not** the final, complete dataset.
